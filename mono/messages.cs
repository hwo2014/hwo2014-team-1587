using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

class ReceivedMsgWrapper
{
    // Suppress "is never assigned" as fields are assigned by reflection
    #pragma warning disable 0649
    public string msgType;
    public Object data;
    public int gameTick;
    public string gameId;
    #pragma warning restore 0649

    public ReceivedMsgWrapper()
    {
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }

    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class CreateRace: SendMsg
{
    public class BotId
    {
        public string name;
        public string key;
    }

    public BotId botId;
    public string trackName;
    public string password;
    public int carCount;

    public CreateRace(string name, string key, string trackName, string password, int carCount)
    {
        this.botId = new BotId();
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "createRace";
    }
}

class JoinRace: SendMsg
{
    public class BotId
    {
        public string name;
        public string key;
    }

    public BotId botId;
    public string trackName;
    public string password;
    public int carCount;

    public JoinRace(string name, string key, string trackName, string password, int carCount)
    {
        this.botId = new BotId();
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

class Ping: SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle: SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class SwitchLane: SendMsg
{
    public string value;

    public SwitchLane(bool left)
    {
        this.value = left ? "Left" : "Right";
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class Turbo: SendMsg
{
    public string value;

    public Turbo(string message)
    {
        this.value = message;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}

