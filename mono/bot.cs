using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

public class Bot
{
    public static string pathSeparator = Path.DirectorySeparatorChar.ToString();
    private const double SpeedOfLight = 300000;

    static string host;
    static int port;
    static string botName;
    static string botKey;
    static string password = "123";

    // **** Settings ****

    // Track to test locally. Must be committed as empty for CI runs
    static string trackName =
        "";
        //"keimola";
        //"germany";
        //"usa";
        //"france";
        //"england";
        //"imola";
        //"elaeintarha";
        //"suzuka";
    static bool createRace = true; // Set to false if someone else creates the race.
    static int botCount = 1;
    static int otherCarCount = 0;

    static int carCount = botCount + otherCarCount;

    // These are not const to get rid of unreachable code warnings
    static bool runTests = false;

    static bool turboEnabled = true;
    static bool laneSwitchEnabled = true;

    static int? allowedSwitchesForDebugging = null; // commit as null
    static bool switchAlwaysForDebugging = false; // commit as false

    static bool dumpTrackToDisk = false;
    static bool writeCSV = false;
    static bool writeLog = trackName != "";

    // The test tracks seem to have the same physics on Prost
    // static double? fixedSpeed = 3.520; // Speed for cruising  40 corner with 0 angle
    // static double? fixedSpeed = 4.390; // Speed for cruising  60 corner with 0 angle
    // static double? fixedSpeed = 5.288; // Speed for cruising  90 corner with 0 angle
    // static double? fixedSpeed = 5.933; // Speed for cruising 110 corner with 0 angle
    // static double? fixedSpeed = 7.580; // Speed for cruising 180 corner with 0 angle
    // static double? fixedSpeed = 8.000; // Speed for cruising 200 corner with 0 angle
    // static double? fixedSpeed = 8.400; // Speed for cruising 220 corner with 0 angle

    // static double? fixedSpeed = 6.63; // Speed to hit angle 60 at end of 180deg long 90 radius
    // static double? fixedSpeed = 6.99; // Speed to hit angle 60 at end of 90deg long 90 radius
    // static double? fixedSpeed = 8.3; // Speed to hit angle 60 at end of 45deg long 90 radius (this is not exact, since cannot reach target speed)

    // static double? fixedSpeed = 7.23; // Speed to hit angle 60 at end of 180deg long 110 radius (linear approx from 90deg corner = 7.16)
    // static double? fixedSpeed = 7.55; // Speed to hit angle 60 at end of 90deg long 110 radius

    // static double? fixedSpeed = 6.30; // Speed to hit angle 60 at end of 90deg long 60 radius -- not exact as not enough time to accel
    // static double? fixedSpeed = 6.30; // Speed to hit angle 60 at end of 90deg long 40 radius -- not exact as not enough time to accel

    static double? fixedSpeed = null;

    // **** Instance members ****

    private double zeroAngleCorneringConstant = 0.5578; // 0.557 - 0.565; // This is on prost (constant physics)

    private StreamWriter dataLogger;

    private CarPosition currentCarPosition;
    private CarPosition previousTickCarPosition;
    private bool switchSentThisPiece;


    public StreamWriter writer;
    private string myName;
    private string myColor;
    private int botIndex;

    private int sentSwitches;

    // read in from incoming message, carPositions
    private int currentTick;

    private double targetThrottle = 0.0;

    public Race race;

    private Dictionary<string, CarPosition> carPositions = new Dictionary<string, CarPosition>();

    private CarPosition carToOvertake;

    private bool raceStarted = false;
    private bool isInQualification = true;

    private Sample sample;

    private List<Straight> longestStraights;


    public double engineAcceleration = 0.2;
    public double friction = 0.02;
    //public double estimatedSpeed = 0.0; // Only used for printing.
    public double estimatedAngle = 0.0; // Only used for printing.
    private double crashAngle = 55.0;


    public static void Main(string[] args)
    {
        if (args == null || args.Length < 4)
        {
            Console.WriteLine("No command line arguments, using defaults.");
            host = "prost.helloworldopen.com"; // constant physics on prost
            // host = "hakkinen.helloworldopen.com";
            // host = "senna.helloworldopen.com";
            // host = "webber.helloworldopen.com";
            // host = "testserver.helloworldopen.com";
            port = 8091;
            botName = "Shallow Orange";
            botKey = "x99FtHDfJRACaw";
        }
        else
        {
            host = args[0];
            port = int.Parse(args[1]);
            botName = args[2];
            botKey = args[3];
        }

        if (runTests)
        {
            Tests.Run();
            return;
        }
        else
        {
            for (int botIndex = 0; botIndex < botCount; ++botIndex)
            {
                var bot = new Bot();
                var currentBotName = botName + ((botIndex > 0) ? "_" + (botIndex + 1).ToString() : "");

                {
                    SendMsg joinMessage;

                    if (carCount == 1)
                    {
                        if (string.IsNullOrEmpty(trackName))
                        {
                            joinMessage = new Join(botName, botKey);
                        }
                        else
                        {
                            joinMessage = new JoinRace(botName, botKey, trackName, password, carCount);
                        }
                    }
                    else
                    {
                        if (botIndex == 0 && createRace)
                        {
                            joinMessage = new CreateRace(currentBotName, botKey, trackName, password, carCount);
                        }
                        else
                        {
                            joinMessage = new JoinRace(currentBotName, botKey, trackName, password, carCount);
                        }
                    }

                    if (botCount == 1)
                    {
                        bot.MainLoop(currentBotName, joinMessage, botIndex);
                    }
                    else
                    {
                        var localBotIndex = botIndex;
                        Thread t = new Thread(new ThreadStart(new Action(() => bot.MainLoop(currentBotName, joinMessage, localBotIndex))));
                        t.Start();
                    }
                }
            }
        }
    }

    public static void Log(string message)
    {
        Console.WriteLine(message);
    }

    private void Init()
    {
        race.Initialize();

        longestStraights = FindLongestStraights(race.track);
    }

    public Bot()
    {
    }

    private int GetPieceCount()
    {
        return race.track.pieces.Length;
    }

    public bool IsInRace
    {
        get
        {
            return !isInQualification;
        }
    }

    public bool IsInQualification
    {
        get
        {
            return isInQualification;
        }
    }

    private double GetConstantPhysicsZeroAngleCornerSpeed(double cornerRadius)
    {
        // sqrt model
        var result = zeroAngleCorneringConstant * Math.Sqrt(cornerRadius);
        return result;
    }

    private double GetTargetSpeed()
    {
        if (currentTick < 3)
        {
            return SpeedOfLight;
        }

        var track = race.track;
        var currentPiece = track.pieces[currentCarPosition.piecePosition.pieceIndex];

        // Speed for current corner
        var targetSpeed = GetCurrentPieceTargetSpeed();
        var carPosition = carPositions[myColor];
        int lane = carPosition.piecePosition.lane.endLaneIndex;
        int lap = carPosition.piecePosition.lap;

        // How far to look ahead
        var breakingDistanceAlmostStop = GetBrakingDistance(currentCarPosition.speed, 2.0);

        // Current piece is covered
        var currentPieceDistance = track.GetPieceLength(currentPiece, currentCarPosition.piecePosition.lane.startLaneIndex, currentCarPosition.piecePosition.lane.endLaneIndex);
        var coveredDistance = currentPieceDistance - currentCarPosition.piecePosition.inPieceDistance;

        // Check next pieces
        while (coveredDistance < breakingDistanceAlmostStop)
        {
            var nextPiece = track.GetNextPiece(currentPiece.Index);

            var lapChanged = nextPiece.Index < currentPiece.Index;
            var finishedRace = (lapChanged && IsLastLap(lap));
            if (finishedRace)
            {
                // No need to worry what happens after finish line
                break;
            }

            currentPiece = nextPiece;

            var approachSpeed = GetApproachSpeed(currentPiece.Index, lane, coveredDistance);
            targetSpeed = Math.Min(targetSpeed, approachSpeed);

            // TODO: handle line changing here
            coveredDistance += track.GetPieceLength(currentPiece, lane, lane);
        }

        Log("Piece " + currentPiece.Index + " final target speed: " + targetSpeed);
        return targetSpeed;
    }

    private double GetApproachSpeed(int pieceIndex, int lane, double atDistance)
    {
        double result;
        var cornerRadius = race.track.GetPieceRadius(pieceIndex, lane);
        if (cornerRadius == 0)
        {
            // Straights do not need iteration
            result = SpeedOfLight;
        }
        else
        {
            var cornerTargetSpeed = GetNewCornerTargetSpeed(pieceIndex, lane);

            // step from cornerTargetSpeed up using minAcceleration
            var iteratedSpeed = cornerTargetSpeed;
            var iteratedDistance = 0.0;
            while (iteratedDistance < atDistance)
            {
                iteratedSpeed = GetPreviousTickSpeed(iteratedSpeed, 0.0, 1.0);
                iteratedDistance += iteratedSpeed;
            }

            result = iteratedSpeed;
        }
        return result;
    }

    private double GetCurrentPieceTargetSpeed()
    {
        var car = carPositions[myColor];
        int pieceIndex = car.piecePosition.pieceIndex;
        int lane = car.piecePosition.lane.endLaneIndex;

        if (!race.track.pieces[pieceIndex].IsCorner)
        {
            // Straights do not need iteration
            return SpeedOfLight;
        }
        else
        {
            double inPieceDistance = car.piecePosition.inPieceDistance;
            double angle = car.angle;
            double angleDelta = car.DeltaAngle;
            double angleDeltaDelta = car.DeltaDeltaAngle;
            return GetCornerMaxSpeed(pieceIndex, lane, inPieceDistance, angle, angleDelta, angleDeltaDelta);
        }
    }

    private double GetNewCornerTargetSpeed(int pieceIndex, int lane)
    {
        double speed = GetCornerMaxSpeed(pieceIndex, lane, 0.0, 0.0, 0.0, 0.0);
        Log("Corner " + pieceIndex + " target speed: " + speed);
        return speed;
    }

    private double GetCornerMaxSpeed(int pieceIndex, int lane, double inPieceDistance, double angle, double angleDelta, double angleDeltaDelta)
    {
        double speed = 20.0;
        while(speed > 0.5)
        {
            if(!WillItCrashWithConstantSpeed(race.track, pieceIndex, inPieceDistance, lane, angle, angleDelta, angleDeltaDelta, speed))
            {
                break;
            }

            speed -= 0.1;
        }

        return speed;
    }

    private double MaxAcceleration(double currentSpeed, double turboFactor)
    {
        var nextSpeed = GetNextTickSpeed(currentSpeed, 1.0, turboFactor);
        var acceleration = nextSpeed - currentSpeed;
        return acceleration;
    }

    private double MinAcceleration(double currentSpeed, double turboFactor)
    {
        var nextSpeed = GetNextTickSpeed(currentSpeed, 0.0, turboFactor);
        var acceleration = nextSpeed - currentSpeed;
        return acceleration;
    }

    private double GetBrakingDistance(double startSpeed, double endSpeed)
    {
        if (endSpeed >= startSpeed)
        {
            return 0;
        }

        var distance = 0.0;
        var iteratedSpeed = startSpeed;
        while (iteratedSpeed > endSpeed)
        {
            iteratedSpeed = GetNextTickSpeed(iteratedSpeed, 0.0, 1.0);
            distance += iteratedSpeed;
        }

        return distance;
    }

    private static double EstimateNextAngle(Track track, int pieceIndex, int lane, double speed, double angle, double angleDelta, double angleDeltaDelta)
    {
        var piece = track.pieces[pieceIndex];
        double radius = track.GetPieceRadius(pieceIndex, lane);
        bool isRightTurn = piece.angle >= 0;
        double k = (isRightTurn ? 1.0 : -1.0);

        double nextAngle;
        if(radius > 0.0)
        {
            double a = 0.99257;
            double b = 0.90310;
            double c = 0.12725;
            double d = 2.02290;
            double e = -0.56629;
            double Cf = speed * speed / radius;
            nextAngle = a * angle * k + b * angleDelta * k + c * angleDeltaDelta * k + d * Cf + e;
        }
        else
        {
            double a = 9.9694e-01;
            double b = 9.0444e-01;
            double c = -4.7149e-03;
            double d = -3.9016e-03;
            double e = -7.8521e-04;
            nextAngle = a * angle * k + b * angleDelta * k + c * angleDeltaDelta + d + angle * speed * e;
        }

        return nextAngle * k;
    }

    private double CalculateTargetThrottle()
    {
        var carPosition = carPositions[this.myColor];

        double speed = carPosition.speed;
        double targetSpeed;
        if (fixedSpeed.HasValue)
        {
            targetSpeed = fixedSpeed.Value;
        }
        else
        {
            targetSpeed = GetTargetSpeed();
        }

        double throttle;
        if(speed > targetSpeed)
        {
            throttle = 0.0;

            // Moar finesse.
            var minAcceleration = MinAcceleration(speed, carPosition.CurrentTurboFactor());
            if (speed + minAcceleration < targetSpeed)
            {
                // Decelerate less
                var maxAcceleration = MaxAcceleration(speed, carPosition.CurrentTurboFactor());
                var desiredAcceleration = targetSpeed - speed;
                throttle = (desiredAcceleration - minAcceleration) / (maxAcceleration - minAcceleration);
            }
        }
        else
        {
            throttle = 1.0;

            // Moar finesse.
            var maxAcceleration = MaxAcceleration(speed, carPosition.CurrentTurboFactor());
            if (speed + maxAcceleration > targetSpeed)
            {
                // Accelerate less
                var minAcceleration = MinAcceleration(speed, carPosition.CurrentTurboFactor());
                var desiredAcceleration = targetSpeed - speed;
                throttle = (desiredAcceleration - minAcceleration) / (maxAcceleration - minAcceleration);
            }
        }

        // If above calculations go wrong correct to valid range.
        var correctedThrottle = Math.Min(1.0, Math.Max(0, throttle));
        if (correctedThrottle != throttle)
        {
            Log("INTERNAL ERROR: throttle calculated as " + throttle);
            throttle = correctedThrottle;
        }

        if(currentTick < 3 && isInQualification)
        {
            throttle = 1.0;
        }

        if (carToOvertake != null &&
            carToOvertake.piecePosition.pieceIndex == carPosition.piecePosition.pieceIndex &&
            carToOvertake.piecePosition.lane.startLaneIndex == carPosition.piecePosition.lane.startLaneIndex &&
            carToOvertake.piecePosition.lane.endLaneIndex == carPosition.piecePosition.lane.endLaneIndex)
        {
            if (throttle != 1.0)
            {
                Log(myName + " rymyää!");
                throttle = 1.0;
            }
        }

        return throttle;
    }

    public bool WillItCrashWithConstantSpeed(Track track, int pieceIndex, double inPieceDistance, int lane, double angle, double angleDelta, double angleDeltaDelta, double speed)
    {
        bool willCrash = false;
        var currentPiece = track.pieces[pieceIndex];
        if (currentPiece.IsCorner)
        {
            var pieceLength = track.GetPieceLength(pieceIndex, lane, lane);

            // Step through the corner with current speed

            var breakingDistanceAlmostStop = GetBrakingDistance(speed, 2.0);
            double d = 0.0;

            willCrash = false;
            while (true)
            {
                double newAngle = EstimateNextAngle(track, pieceIndex, lane, speed, angle, angleDelta, angleDeltaDelta);
                double newAngleDelta = newAngle - angle;
                double newAngleDeltaDelta = newAngleDelta - angleDelta;
                if (Math.Abs(newAngle) >= crashAngle)
                {
                    willCrash = true;
                    break;
                }
                inPieceDistance += speed;
                d += speed;

                angle = newAngle;
                angleDelta = newAngleDelta;
                angleDeltaDelta = newAngleDeltaDelta;

                //Log("WillItCrash? pieceIndex:" + pieceIndex + ", inPieceDistance: " + inPieceDistance + ", angle: " + angle);

                // Check all connected corner pieces.
                if(inPieceDistance >= pieceLength)
                {
                    inPieceDistance -= pieceLength;
                    pieceIndex = (pieceIndex + 1) % track.pieces.Length;
                    currentPiece = track.pieces[pieceIndex];
                    pieceLength = track.GetPieceLength(pieceIndex, lane, lane);
                    if (d >= breakingDistanceAlmostStop)
                    {
                        break;
                    }
                }
            }
        }
        return willCrash;
    }

    private void UpdateCarPositions(JArray dataJArray)
    {
        foreach (var jObject in dataJArray.OfType<JObject>())
        {
            var newCarPosition = jObject.ToObject<CarPosition>();
            CarPosition oldCarPosition;
            if (carPositions.TryGetValue(newCarPosition.id.color, out oldCarPosition))
            {
                // Copy manually our own members.
                newCarPosition.crashed = oldCarPosition.crashed;
                newCarPosition.turbos = oldCarPosition.turbos; // Note that turbos are not copied.
                foreach(var x in newCarPosition.turbos)
                {
                    if(x.active)
                    {
                        --x.duration;
                    }
                }
                newCarPosition.RaceDistanceTravelledOnTicks = oldCarPosition.RaceDistanceTravelledOnTicks;

                // Update speed.
                {
                    double d = race.track.GetDistance(oldCarPosition.piecePosition, newCarPosition.piecePosition);
                    newCarPosition.travelledDistance = oldCarPosition.travelledDistance + d;
                    newCarPosition.previousSpeed = oldCarPosition.speed;
                    newCarPosition.speed = d;
                }

                // Prev angle for delta angle
                newCarPosition.previousAngle = oldCarPosition.angle;
                newCarPosition.previousDeltaAngle = oldCarPosition.DeltaAngle;
                newCarPosition.bestLapTimeMs = oldCarPosition.bestLapTimeMs;
                newCarPosition.previousDeltaDeltaAngle = oldCarPosition.DeltaDeltaAngle;
                newCarPosition.previousPieceIndex = oldCarPosition.piecePosition.pieceIndex;
                newCarPosition.previousStartLane = oldCarPosition.piecePosition.lane.startLaneIndex;
            }
            else
            {
                newCarPosition.travelledDistance = 0.0;
                newCarPosition.speed = 0.0;
                newCarPosition.RaceDistanceTravelledOnTicks = new List<double>();
            }

            carPositions[newCarPosition.id.color] = newCarPosition;
        }

        // Keep log on car positions over ticks.
        foreach (var newCarPosition in carPositions.Values)
        {
            // Fill possible gaps in race travelled
            var raceDistanceTravelledOnTicks = newCarPosition.RaceDistanceTravelledOnTicks;
            var lastRaceTravelled = (raceDistanceTravelledOnTicks.Count > 0) ?
                raceDistanceTravelledOnTicks[raceDistanceTravelledOnTicks.Count - 1] :
                0.0;
            while (raceDistanceTravelledOnTicks.Count < currentTick)
            {
                raceDistanceTravelledOnTicks.Add(lastRaceTravelled);
            }

            // Add current race travelled value.
            raceDistanceTravelledOnTicks.Add(newCarPosition.GetRaceDistanceTravelled(race.track));
        }

        previousTickCarPosition = currentCarPosition;
        currentCarPosition = carPositions[myColor];
    }

    private void FindCarToOverTake()
    {
        this.carToOvertake = null;

        // Find car ahead on track
        var track = race.track;
        CarPosition ourCar = carPositions[myColor];
        var ourLapDistance = ourCar.GetLapDistanceTravelled(track);
        var carAhead = GetCarAhead(ourLapDistance, false);
        if (carAhead != null)
        {
            // If we are closing in, should overtake.
            const int lookBackTicks = 30;
            if (currentTick > lookBackTicks)
            {
                var ourRaceDistanceBefore = track.GetLapCenterlineDistanceFromRaceDistance(ourCar.RaceDistanceTravelledOnTicks[currentTick - lookBackTicks]);
                var carAheadDistanceBefore = track.GetLapCenterlineDistanceFromRaceDistance(carAhead.RaceDistanceTravelledOnTicks[currentTick - lookBackTicks]);
                var differenceBefore = carAheadDistanceBefore - ourRaceDistanceBefore;

                var ourRaceDistanceNow = track.GetLapCenterlineDistanceFromRaceDistance(ourCar.RaceDistanceTravelledOnTicks[currentTick]);
                var carAheadDistanceNow = track.GetLapCenterlineDistanceFromRaceDistance(carAhead.RaceDistanceTravelledOnTicks[currentTick]);

                var lapCenterlineLenght = track.GetLapCenterlineLength();
                var differenceNow = (carAheadDistanceNow - ourRaceDistanceNow + lapCenterlineLenght) % lapCenterlineLenght;

                // TODO: Maybe some tolerance here
                if (differenceNow < differenceBefore)
                {
                    this.carToOvertake = carAhead;
                }
            }
        }
    }

    private CarPosition GetCarAhead(double ourLapDistance, bool allowCrashed)
    {
        CarPosition carAhead = null;
        double closestAheadDistance = 300.0; // Care only about cars near
        foreach (var carPosition in carPositions.Values)
        {
            string carColor = carPosition.id.color;
            if (!string.Equals(carColor, myColor) && (allowCrashed || !carPosition.crashed))
            {
                // Other car
                var otherCarLapDistance = carPosition.GetLapDistanceTravelled(race.track);
                var aheadDistance = otherCarLapDistance - ourLapDistance;
                if (aheadDistance < 0)
                {
                    var lapDistance = race.track.GetLapCenterlineLength();
                    aheadDistance += lapDistance;
                }

                if (aheadDistance < closestAheadDistance)
                {
                    closestAheadDistance = aheadDistance;
                    carAhead = carPosition;
                }
            }
        }
        return carAhead;
    }

    private bool ShouldSendTurbo()
    {
        if(!turboEnabled)
        {
            // Turbo's disabled.
            return false;
        }

        var car = carPositions[myColor];
        if(car.crashed || car.TurbosAvailable() <= 0)
        {
            // Can't turbo.
            return false;
        }

        var availableTurbo = car.AvailableTurbo;
        Debug.Assert(availableTurbo != null);

        if(car.CurrentTurboFactor() > 1.0)
        {
            // Turbo already active, can't turbo.
            return false;
        }

        if(car.turbos[0].factor < 1.0)
        {
            // The server is trolling us.
            return false;
        }

        var pieceIndex = car.piecePosition.pieceIndex;
        if(longestStraights.Count > 0 && longestStraights[0].index == pieceIndex)
        {
            return true;
        }

        if (IsLastLap(car.piecePosition.lap))
        {
            var track = race.track;

            // Is final piece
            if (pieceIndex == track.pieces.Length)
            {
                return true;
            }

            // Is final straight
            var mergedPiece = race.track.GetMergedPiece(race.track.pieces[pieceIndex]);
            if (mergedPiece.IsStraight && mergedPiece.mergedPieceEndIndex < mergedPiece.Index)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsLastLap(int lap)
    {
        return race.raceSession.laps.HasValue && lap == (race.raceSession.laps.Value - 1);
    }

    private void DeduceAccelerationValues()
    {
        if(currentTick < 3 && isInQualification)
        {
            var car = carPositions[myColor];
            if(currentTick == 0)
            {
                Log("currentTick 0, speed: " + car.speed);
            }
            else if(currentTick == 1)
            {
                Log("currentTick 1, speed: " + car.speed);
                engineAcceleration = car.speed;
            }
            else if(currentTick == 2)
            {
                Log("currentTick 2, speed: " + car.speed);

                // Assume that: drag = f * v => f = drag / v.
                double v_1 = engineAcceleration;
                double v_2 = car.speed;
                double v_2_withoutDrag = v_1 + engineAcceleration;
                double drag = v_2_withoutDrag - v_2;
                friction = drag / v_1;

                Log("Friction: " + friction);
                Log("Engine Acceleration: " + engineAcceleration);
            }
        }
    }

    public void ProcessLine(String line)
    {
        ReceivedMsgWrapper msg;

        try
        {
            msg = JsonConvert.DeserializeObject<ReceivedMsgWrapper>(line);
            ProcessMessage(msg);
        }
        catch(Exception ex)
        {
            Log("Error in processing line: '" + line + "' " + ex);
            SendPing();
        }
    }

    private void ProcessMessage(ReceivedMsgWrapper msg)
    {
        switch (msg.msgType)
        {
            case "carPositions":
                {
                    this.currentTick = msg.gameTick;

                    UpdateCarPositions(msg.data as JArray);
                    FindCarToOverTake();
                    DeduceAccelerationValues();

                    var throttle = (currentTick < 3) ? 1.0 : CalculateTargetThrottle();
                    if(this.targetThrottle != throttle && !writeCSV)
                    {
                        Log(myName + ": Set throttle to " + throttle);
                    }
                    this.targetThrottle = throttle;

                    if(writeCSV)
                    {
                        WriteCSV();
                    }
                    else
                    {
                        PrintCarPositions();
                    }

                    if (previousTickCarPosition != null)
                    {
                        if (previousTickCarPosition.piecePosition.pieceIndex != currentCarPosition.piecePosition.pieceIndex)
                        {
                            switchSentThisPiece = false;
                        }
                    }

                    // After each carPositions message with a gameTick, reply with either a) throttle, b) laneSwitch, c) ping, or d) turbo.
                    if(raceStarted)
                    {
                        bool sendSwitch = false;
                        bool sendSwitchToLeft = false;
                        var currentPiece = race.track.pieces[currentCarPosition.piecePosition.pieceIndex];
                        var currentLane = race.track.lanes[currentCarPosition.piecePosition.lane.endLaneIndex];

                        if (currentCarPosition.piecePosition.lane.startLaneIndex != currentCarPosition.piecePosition.lane.endLaneIndex)
                        {
                        }

                        var nextPiece = race.track.pieces[(currentCarPosition.piecePosition.pieceIndex + 1) % race.track.pieces.Length];
                        var nextNextPiece = race.track.pieces[(currentCarPosition.piecePosition.pieceIndex + 2) % race.track.pieces.Length];

                        if (!switchSentThisPiece && nextPiece.hasSwitch == true)
                        {
                            // If there is car to overtake, naively switch to another line as the car
                            KeyValuePair<Track.Piece, Track.Lane>? bannedPath = null;
                            if (carToOvertake != null)
                            {
                                bannedPath = new KeyValuePair<Track.Piece, Track.Lane>(nextNextPiece, race.track.lanes[carToOvertake.piecePosition.lane.endLaneIndex]);
                            }

                            var startVertex = GetShortestPath(race.track, nextPiece, currentLane, currentPiece, currentLane, bannedPath);
                            Debug.Assert(startVertex.Piece == nextPiece && startVertex.Lane == currentLane);

                            // "A positive value tells that the lanes is to the right from the center line while a negative value indicates a lane to the left from the center."
                            if (startVertex.Next.Lane.distanceFromCenter < currentLane.distanceFromCenter)
                            {
                                sendSwitch = true;
                                sendSwitchToLeft = true;
                            }
                            else if (startVertex.Next.Lane.distanceFromCenter > currentLane.distanceFromCenter)
                            {
                                sendSwitch = true;
                                sendSwitchToLeft = false;
                            }

                            if (switchAlwaysForDebugging)
                            {
                                sendSwitch = true;
                            }
                        }

                        if (sendSwitch && laneSwitchEnabled && (allowedSwitchesForDebugging == null || sentSwitches < allowedSwitchesForDebugging))
                        {
                            sentSwitches++;
                            switchSentThisPiece = true;
                            SendSwitch(sendSwitchToLeft);
                        }
                        else if(ShouldSendTurbo())
                        {
                            var availableTurbo = currentCarPosition.AvailableTurbo;
                            if (availableTurbo != null)
                            {
                                availableTurbo.triedToActivate = true;
                            }
                            SendTurbo();
                        }
                        else
                        {
                            SendThrottle();
                        }
                    }
                }
                break;
            case "crash":
                {
                    var data = msg.data as JObject;
                    string color = data["color"].ToString();
                    string name = data["name"].ToString();

                    var carPosition = carPositions[color];
                    int lane = carPosition.piecePosition.lane.endLaneIndex;
                    int pieceIndex = carPosition.piecePosition.pieceIndex;
                    double inPieceDistance = carPosition.piecePosition.inPieceDistance;
                    double speed = carPosition.speed;

                    Log("Car crashed: " + name + "/" + color + " at  lane " + lane + "  piece "  + pieceIndex + "  dist " + inPieceDistance + "  speed " + speed);
                    carPosition.Crash();

                    if (color == this.myColor)
                    {
                        // It was us who crashed.
                        if(isInQualification)
                        {
                            crashAngle -= 4.0;
                            Log("Crashed, new crash angle: " + crashAngle);
                        }
                    }
                }
                break;
            case "spawn":
                {
                    var data = msg.data as JObject;
                    string color = data["color"].ToString();
                    string name = data["name"].ToString();

                    Log("Car spawned: " + name + "/" + color);
                    var carPosition = carPositions[color];
                    carPosition.crashed = false;

                    if (color == this.myColor)
                    {
                        // It was us who spawned.
                    }
                }
                break;
            case "join":
                {
                    Log("Joined");
                    //SendPing();
                }
                break;
            case "gameInit":
                {
                    var data = msg.data as JObject;
                    this.race = data["race"].ToObject<Race>();

                    // When racing alone we are in qualification mode and also when duration ms is set
                    this.isInQualification = race.cars.Length == 1 || race.raceSession.IsQualification;

                    Init();

                    if (dumpTrackToDisk)
                    {
                        File.WriteAllLines(".." + pathSeparator + "tracks" + pathSeparator + trackName + ".track", new string[] { data["race"].ToString() });
                    }

                    if (this.isInQualification)
                    {
                        Log("Race init - qualification");
                    }
                    else
                    {
                        Log("Race init - race");
                    }
                    race.Print();

                    //SendPing();
                }
                break;
            case "gameEnd":
                {
                    // This might be end of qualifying or race
                    Log("Race ended");
                    SendPing();

                    raceStarted = false;
                    isInQualification = false;
                }
                break;
            case "gameStart":
                {
                    Log("Race starts");
                    raceStarted = true;

                    // Note that sending throttle here does do something now!
                    SendThrottle();
                }
                break;
            case "yourCar":
                {
                    Log("yourCar received");
                    var dataJObject = msg.data as JObject;
                    this.myName = dataJObject["name"].ToString();
                    this.myColor = dataJObject["color"].ToString();
                    Log("  My name: " + this.myName);
                    Log("  My color: " + this.myColor);
                    //SendPing();
                }
                break;
            case "lapFinished":
                {
                    var data = msg.data as JObject;

                    var car = data["car"];
                    string name = car["name"].ToString();
                    string color = car["color"].ToString();

                    var lapTime =  data["lapTime"].Value<JObject>()["millis"].Value<int>();
                    var raceTime = data["raceTime"];
                    var ranking = data["ranking"];

                    Log("Car finished a lap: " + name + "/" + color);
                    Log("" + lapTime + " " + raceTime + " " + ranking);

                    CarPosition carPosition;
                    if (carPositions.TryGetValue(color, out carPosition))
                    {
                        if (lapTime < carPosition.bestLapTimeMs)
                        {
                            carPosition.bestLapTimeMs = lapTime;
                        }
                    }
                }
                break;
            case "finish":
                {
                    var data = msg.data as JObject;
                    string name = data["name"].ToString();
                    string color = data["color"].ToString();

                    Log("Car finished the race: " + name + "/" + color);
                }
                break;
            case "dnf":
                {
                    var data = msg.data as JObject;

                    var car = data["car"];
                    string name = car["name"].ToString();
                    string color = car["color"].ToString();

                    string reason = data["reason"].ToString();

                    carPositions.Remove(color);

                    Log("Car disqualified: " + name + "/" + color + ", reason: " + reason);
                }
                break;
            case "tournamentEnd":
                {
                    Log("Tournament ended");
                    Send(new Ping());
                }
                break;
            case "error":
                {
                    Log("Got error message: " + msg.data);
                    Send(new Ping());
                }
                break;
            case "turboAvailable":
                {
                    var data = msg.data as JObject;

                    int duration = data.Value<int>("turboDurationTicks");
                    double factor = data.Value<double>("turboFactor");
                    foreach(var entry in carPositions)
                    {
                        entry.Value.AddTurbo(duration, factor);
                    }

                    Log("Got turbo, duration: " + duration + ", engine factor: " + factor);
                }
                break;
            case "turboStart":
                {
                    var data = msg.data as JObject;
                    string name = data["name"].ToString();
                    string color = data["color"].ToString();

                    carPositions[color].StartTurbo();

                    Log("Car " + name + "/" + color + " started turbo!");
                    break;
                }
            case "turboEnd":
                {
                    var data = msg.data as JObject;
                    string name = data["name"].ToString();
                    string color = data["color"].ToString();

                    carPositions[color].EndTurbo();

                    Log("Car " + name + "/" + color + " ended turbo");
                    break;
                }
            case "createRace":
                {
                    // Server will echo this
                    Log("createRace got, waiting for gameInit");
                    break;
                }
            case "joinRace":
                {
                    // Server will echo this
                    Log("joinRace got, waiting for gameInit");
                    break;
                }
            case "raceSession":
                {
                    Log("raceSession got");
                    break;
                }
            default:
                {
                    Log("Got unknown message: " + msg.msgType + " " + msg.data);
                    Send(new Ping());
                }
                break;
        }
    }

    private void MainLoop(string botName, SendMsg joinMessage, int botIndex)
    {
        this.myName = botName;
        this.botIndex = botIndex;

        if (botIndex > 0)
        {
            // Wait for the first bot to send createRace first
            Thread.Sleep(2000 + botIndex * 500);
        }

        Log("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            this.writer = writer;
            string line;

            Send(joinMessage);

            string stamp = DateTime.UtcNow.ToString("yyyy-dd-MM_HH-mm-ss_" + ((botIndex > 0) ? "_bot" + (botIndex + 1) : ""));
            if (writeCSV && botIndex == 0)
            {
                string filename = "data_log_" + stamp + ".txt";
                Log("Writing CSV log to '" + filename + "'");
                this.dataLogger = new StreamWriter(filename);
            }

            var serverLogFileName = "last_server_log" + ((botIndex > 0) ? "_bot" + (botIndex + 1) : "") + ".txt";
            StreamWriter logger = null;
            if (writeLog && botIndex == 0)
            {
                try
                {
                    logger = new StreamWriter(serverLogFileName);
                }
                catch(Exception ex)
                {
                    Log("Error opening server log: '" + serverLogFileName + "' " + ex);
                }
            }

            {
                while ((line = reader.ReadLine()) != null)
                {
                    if (logger != null)
                    {
                        logger.WriteLine(line);
                        logger.Flush();
                    }

                    ProcessLine(line);
                }
            }

            if (logger != null)
            {
                logger.Close();
            }

            if(dataLogger != null)
            {
                dataLogger.Close();
            }
        }
        Log("Bot " + botName + " exited");
    }

    private void Send(SendMsg msg)
    {
        var json = msg.ToJson();

        //Log("Send line: " + json);
        writer.WriteLine(json);
    }

    private void SendPing()
    {
        Send(new Ping());
    }

    private void SendThrottle()
    {
        Send(new Throttle(targetThrottle));
    }

    private void SendSwitch(bool left)
    {
        Send(new SwitchLane(left));
    }

    private void SendTurbo()
    {
        Log("Warp speed!");
        Send(new Turbo("Jaffat tulloo, pois alta!"));
    }

    public class Sample
    {
        public int tick;
        public double throttle;
        public double speed;
        public double angle;
        public double cornerRadius;
        public string trackName;

        public double nextSpeed;
        public double nextAngle;
    }


    private void PrintCarPositions()
    {
        var myCar = carPositions[myColor];
        foreach(var entry in carPositions)
        {
            var car = entry.Value;
            if (car == myCar)
            {
                car.Print(car == myCar ? targetThrottle : -9.0);
            }
        }

        //double speedEstimateError = myCar.speed - estimatedSpeed;
        //double myNextSpeed = myCar.speed + (engineAcceleration * targetThrottle) - (friction * myCar.speed);
        //Log("tick: " + currentTick + ", speed: " + myCar.speed + ", last speed estimate error: " + speedEstimateError + ", throttle: " + targetThrottle + ", estimated next speed: " + myNextSpeed);

        double angleEstimateError = myCar.angle - estimatedAngle;
        double myNextAngle = EstimateNextAngle(race.track, myCar.piecePosition.pieceIndex, myCar.piecePosition.lane.startLaneIndex, myCar.speed, myCar.angle, myCar.DeltaAngle, myCar.DeltaDeltaAngle);
        Log(String.Format("tick: {0,4}  angle: {1,-8:0.0000}  last angle estimate error: {2,-8:0.0000}  throttle: {3,-8:0.0000} estimated next angle: {4,-8:0.0000}", currentTick, myCar.angle, angleEstimateError, targetThrottle, myNextAngle));

        //estimatedSpeed = myNextSpeed;
        estimatedAngle = myNextAngle;
    }

    public double GetNextTickSpeed(double currentSpeed, double throttle, double turboFactor)
    {
        double nextSpeed = currentSpeed + (engineAcceleration * turboFactor * throttle) - (friction * currentSpeed);
        return nextSpeed;
    }

    public double GetPreviousTickSpeed(double currentSpeed, double throttle, double turboFactor)
    {
        double previousSpeed = currentSpeed - (engineAcceleration * turboFactor * throttle) + (friction * currentSpeed);
        return previousSpeed;
    }

    private void WriteCSV()
    {
        var car = carPositions[myColor];
        var piece = car.piecePosition;

        if(sample != null)
        {
            ++sample.tick;
            sample.nextSpeed = car.speed;
            sample.nextAngle = car.angle;

            /*
            string line = String.Format(
                "{0}\t{1:0.0000}\t{2:0.0000}\t{3:0.0000}\t{4}\t{5}\t{6:0.0000}\t{7:0.0000}\t{8:0.0000}\t{9:0.0000}\t{10}\t{11:0.0000}",
                sample.tick,
                sample.throttle,
                sample.speed,
                sample.angle,
                sample.cornerRadius,
                sample.trackName,
                sample.nextSpeed,
                sample.nextAngle,
                sample.nextSpeed - sample.speed,
                sample.nextAngle - sample.angle,
                car.piecePosition.pieceIndex,
                car.piecePosition.inPieceDistance);*/

            var prevCornerRadius = race.track.GetPieceRadius(car.previousPieceIndex, car.previousStartLane);
            var prevPiece = race.track.pieces[car.previousPieceIndex];
            bool isRightTurn = prevPiece.angle >= 0;
            //if (prevCornerRadius > 0)
            if (prevCornerRadius == 0.0 && Math.Abs(car.previousAngle) > 0.0)
            {
                double k = (isRightTurn ? 1.0 : -1.0);
                string line = String.Format(
                              "{0}\t{1}\t{2}\t{3}\t{4}\t{5}",
                              car.angle * k,
                              car.previousAngle * k,
                              car.previousDeltaAngle * k,
                              car.previousDeltaDeltaAngle * k,
                              car.previousSpeed * car.previousSpeed / prevCornerRadius,
                              car.previousSpeed);
                dataLogger.WriteLine(line);
                dataLogger.Flush();
                Log(line);
            }
        }
        else
        {
            sample = new Sample();
            sample.tick = 0;
        }

        sample.throttle = targetThrottle;
        sample.speed = car.speed;
        sample.angle = car.angle;
        sample.cornerRadius = race.track.GetPieceRadius(piece.pieceIndex, piece.lane.startLaneIndex);
        sample.trackName = Bot.trackName;
    }

    public static double GetDijkstraWeight(Track track, Track.Piece piece, Track.Lane startLane, Track.Lane endLane)
    {
        // TODO: calculate time to pass the piece on lane
        // Possible take account current speed, after crash etc.
        return track.GetPieceLength(piece.Index, startLane.index, endLane.index);
    }

    public class Tuple<T,U>
    {
        public T Item1 { get; private set; }
        public U Item2 { get; private set; }

        public Tuple(T item1, U item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        public override bool Equals(object obj)
        {
            var other = (Tuple<T, U>)obj;
            return object.Equals(this.Item1, other.Item1) && object.Equals(this.Item2, other.Item2);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + Item1.GetHashCode();
            hash = hash * 31 + Item2.GetHashCode();
            return hash;
        }
    }

    public static class Tuple
    {
        public static Tuple<T, U> Create<T, U>(T item1, U item2)
        {
            return new Tuple<T, U>(item1, item2);
        }
    }

    public class DijkstraVertex : IComparable<DijkstraVertex>
    {
        public double Distance { get; set; }
        public Track.Piece Piece { get; set; }
        public Track.Lane Lane { get; set; }
        public DijkstraVertex Next { get; set; }
        public List<Tuple<DijkstraVertex, double>> Neighbors;

        public int CompareTo(DijkstraVertex other)
        {
            if (other == null)
            {
                return 1;
            }

            var result = this.Distance.CompareTo(other.Distance);
            return result;
        }

        public DijkstraVertex(Track.Piece piece, Track.Lane lane)
        {
            this.Piece = piece;
            this.Distance = double.PositiveInfinity;
            this.Lane = lane;
        }

        public void AddNeighbors(Dictionary<Tuple<Track.Piece, Track.Lane>, DijkstraVertex> vertices, Track track, KeyValuePair<Track.Piece, Track.Lane>? bannedPath)
        {
            this.Neighbors = new List<Tuple<DijkstraVertex, double>>();

            // Get previous piece
            var previousPieceIndex = (this.Piece.Index - 1 + track.pieces.Length) % track.pieces.Length;
            var previousPiece = track.pieces[previousPieceIndex];

            // Same lane previous piece

            Track.Lane neighborLane;
            Track.Piece neighborPiece;
            DijkstraVertex neightborVertex;

            neighborLane = this.Lane;
            neighborPiece = previousPiece;
            neightborVertex = vertices[Tuple.Create(neighborPiece, neighborLane)];
            if (bannedPath == null || bannedPath.Value.Key != neighborPiece || bannedPath.Value.Value != neighborLane)
            {
                this.Neighbors.Add(Tuple.Create(neightborVertex, GetDijkstraWeight(track, this.Piece, this.Lane, neighborLane)));
            }

            // Adjacent lanes next piece
            if (previousPiece.hasSwitch == true)
            {
                var nextLaneIndex = this.Lane.index + 1;
                if (nextLaneIndex < track.lanes.Length)
                {
                    neighborLane = track.lanes[nextLaneIndex];
                    neighborPiece = previousPiece;
                    neightborVertex = vertices[Tuple.Create(neighborPiece, neighborLane)];
                    if (bannedPath == null || bannedPath.Value.Key != neighborPiece || bannedPath.Value.Value != neighborLane)
                    {
                        this.Neighbors.Add(Tuple.Create(neightborVertex, GetDijkstraWeight(track, this.Piece, this.Lane, neighborLane)));
                    }
                }

                var previousLaneIndex = this.Lane.index - 1;
                if (previousLaneIndex >= 0)
                {
                    neighborLane = track.lanes[previousLaneIndex];
                    neighborPiece = previousPiece;
                    neightborVertex = vertices[Tuple.Create(neighborPiece, neighborLane)];
                    if (bannedPath == null || bannedPath.Value.Key != neighborPiece || bannedPath.Value.Value != neighborLane)
                    {
                        this.Neighbors.Add(Tuple.Create(neightborVertex, GetDijkstraWeight(track, this.Piece, this.Lane, neighborLane)));
                    }
                }
            }
        }
    }

    public static DijkstraVertex GetShortestPath(Track track, Track.Piece startPiece, Track.Lane startLane, Track.Piece endPiece, Track.Lane endLane, KeyValuePair<Track.Piece, Track.Lane>? bannedPath)
    {
        var vertices = new Dictionary<Tuple<Track.Piece, Track.Lane>, DijkstraVertex>();

        DijkstraVertex startVertex = null;
        DijkstraVertex endVertex = null;

        foreach(var piece in track.pieces)
        {
            foreach (var lane in track.lanes)
            {
                var vertex = new DijkstraVertex(piece, lane);

                if (piece == startPiece && lane == startLane)
                {
                    startVertex = vertex;
                }
                if (piece == endPiece && lane == endLane)
                {
                    endVertex = vertex;
                    endVertex.Distance = 0.0;
                }

                vertices.Add(Tuple.Create(piece, lane), vertex);
            }
        }
        foreach(var vertex in vertices.Values)
        {
            vertex.AddNeighbors(vertices, track, bannedPath);
        }

        var visitedVertices = new HashSet<DijkstraVertex>();
        var remainingVertices = new List<DijkstraVertex>();

        foreach(var vertex in vertices.Values)
        {
            remainingVertices.Add(vertex);
        }

        while (remainingVertices.Count > 0)
        {
            var minVertex = remainingVertices.Min();
            var removalSucceeded = remainingVertices.Remove(minVertex);
            Debug.Assert(removalSucceeded);
            visitedVertices.Add(minVertex);

            foreach(var neighborPair in minVertex.Neighbors)
            {
                var edgeWeight = neighborPair.Item2;
                var neighbor = neighborPair.Item1;
                var newNeighborDistance = (minVertex.Distance + edgeWeight);
                if (neighbor.Distance > newNeighborDistance)
                {
                    neighbor.Distance = newNeighborDistance;
                    neighbor.Next = minVertex;
                }
            }
        }

        return startVertex;
    }

    public class Straight
    {
        public int index;
        public double length;
    }

    public static List<Straight> FindLongestStraights(Track track)
    {
        int n = track.pieces.Length;
        var list = new List<Straight>();
        int start = 0;

        // Find any non-straight as starting point to get the goal straight in one piece.
        while(start < n)
        {
            if(track.pieces[start].IsCorner)
            {
                break;
            }
            ++start;
        }
        Straight s = null;
        for(int i = (start + 1) % n; i != start; i = (i + 1) % n)
        {
            // TODO: Include large radius corners as straights.
            if(track.pieces[i].IsCorner)
            {
                if(s != null)
                {
                    list.Add(s);
                    s = null;
                }
                continue;
            }
            else
            {
                if(s == null)
                {
                    s = new Straight();
                    s.index = i;
                    s.length = 0.0;
                }
                s.length += track.GetPieceLengthCenter(i);
            }
        }
        if(s != null)
        {
            list.Add(s);
        }
        list.Sort((x,y) => y.length.CompareTo(x.length));
        return list;
    }
}

