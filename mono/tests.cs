using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

class Tests
{
    public static void Run()
    {
        Tests tests = new Tests();

        tests.TestLaptimeParsing();
        tests.TestSwitchPieceLength();
        tests.TestCornerPieceMerge();
        tests.TestShortestPath();
        tests.LoadTrackOffline();
        tests.TestTrackParsing();
        tests.TestCarPositionsParsing();
        tests.TestTurboFinder();
        tests.TestSimulator();

        Console.WriteLine("");
        Console.WriteLine("**** ALL TESTS PASSED ****");
        Console.WriteLine("");
    }

    private void TestLaptimeParsing()
    {
        var line = @"{""msgType"":""lapFinished"",""data"":{""car"":{""name"":""Shallow Orange_2"",""color"":""blue""},""lapTime"":{""lap"":1,""ticks"":469,""millis"":7816},""raceTime"":{""laps"":2,""ticks"":994,""millis"":16566},""ranking"":{""overall"":4,""fastestLap"":4}},""gameId"":""2d3b55ef-c7a4-4834-8c19-4c0eed84cf24"",""gameTick"":994}";
        var msg = JsonConvert.DeserializeObject<ReceivedMsgWrapper>(line);
        var data = msg.data as JObject;
        var lapTime =  data["lapTime"].Value<JObject>()["millis"].Value<int>();

        Assert(lapTime == 7816);
    }

    private void TestSwitchPieceLength()
    {
        string raceString = File.ReadAllText(".." + Bot.pathSeparator + "tracks" + Bot.pathSeparator + "keimola.track");
        Race race = JsonConvert.DeserializeObject<Race>(raceString);
        race.Initialize();

        var track = race.track;

        Assert(track.pieces[3].hasSwitch == true);
        Assert(track.GetPieceLength(track.pieces[3], 0, 0) == track.GetPieceLength(track.pieces[3], 1, 1));
        Assert(track.GetPieceLength(track.pieces[3], 1, 0) == track.GetPieceLength(track.pieces[3], 0, 1));
        Assert(track.GetPieceLength(track.pieces[3], 0, 0) < track.GetPieceLength(track.pieces[3], 0, 1));
        Assert(track.GetPieceLength(track.pieces[3], 0, 0) < track.GetPieceLength(track.pieces[3], 1, 0));

        Assert(track.pieces[8].hasSwitch == true);
        Assert(track.GetPieceLength(track.pieces[8], 0, 0) > track.GetPieceLength(track.pieces[8], 1, 1));
        // Assert(track.GetPieceLength(track.pieces[8], 1, 0) == track.GetPieceLength(track.pieces[8], 0, 1));
        Assert(track.GetPieceLength(track.pieces[8], 0, 0) > track.GetPieceLength(track.pieces[8], 0, 1));
        Assert(track.GetPieceLength(track.pieces[8], 0, 0) > track.GetPieceLength(track.pieces[8], 1, 0));
        Assert(track.GetPieceLength(track.pieces[8], 1, 1) < track.GetPieceLength(track.pieces[8], 1, 0));
        Assert(track.GetPieceLength(track.pieces[8], 1, 1) < track.GetPieceLength(track.pieces[8], 0, 1));
    }

    private void TestCornerPieceMerge()
    {
        string raceString = File.ReadAllText(".." + Bot.pathSeparator + "tracks" + Bot.pathSeparator + "keimola.track");
        Race race = JsonConvert.DeserializeObject<Race>(raceString);
        race.Initialize();

        var track = race.track;
        Track.Piece mergedPiece;

        mergedPiece = track.GetMergedPiece(track.pieces[0]);
        Assert(track.GetPieceLength(track.pieces[0], 0, 0) == 100);
        Assert(mergedPiece.IsStraight && mergedPiece.length == 400);
        Assert(track.GetPieceLength(mergedPiece, 0, 0) == 400);
        mergedPiece = track.GetMergedPiece(track.pieces[4]);
        Assert(mergedPiece.IsCorner && mergedPiece.angle == 180 && mergedPiece.radius == 100);
        Assert(Math.Abs(track.GetPieceLength(mergedPiece, 0, 0) - Math.PI * 110) < 0.01);
        Assert(track.pieces[8] == track.GetMergedPiece(track.pieces[8]));
    }

    private void TestTrackParsing()
    {
        string line = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Shallow Orange\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"d79d6ce6-ac6e-45b3-80b4-091f7c661c10\"}";
        MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
        var dataJObject = msg.data as JObject;
        Race race = dataJObject["race"].ToObject<Race>();

        race.Print();
    }

    private void TestCarPositionsParsing()
    {
        string message = @"{""msgType"": ""carPositions"", ""data"": [ { ""id"": { ""name"": ""Schumacher"", ""color"": ""red"" }, ""angle"": 0.0, ""piecePosition"": { ""pieceIndex"": 0, ""inPieceDistance"": 0.0, ""lane"": { ""startLaneIndex"": 0, ""endLaneIndex"": 0 }, ""lap"": 0 } }, { ""id"": { ""name"": ""Rosberg"", ""color"": ""blue"" }, ""angle"": 45.0, ""piecePosition"": { ""pieceIndex"": 0, ""inPieceDistance"": 20.0, ""lane"": { ""startLaneIndex"": 1, ""endLaneIndex"": 1 }, ""lap"": 0 } } ], ""gameId"": ""OIUHGERJWEOI"", ""gameTick"": 0}";
        //string message = @"{""msgType"":""carPositions"",""data"":[{""id"":{""name"":""Shallow Orange"",""color"":""red""},""angle"":0.0,""piecePosition"":{""pieceIndex"":0,""inPieceDistance"":12.330441545161477,""lane"":{""startLaneIndex"":0,""endLaneIndex"":0},""lap"":0}}],""gameId"":""09422f78-6931-457f-8c28-57071bae4241"",""gameTick"":16}";
        MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(message);
        var dataJObject = (JArray)msg.data;
        var carPositions = new List<CarPosition>();
        foreach (var jObject in dataJObject.OfType<JObject>())
        {
            var carPosition = jObject.ToObject<CarPosition>();
            carPosition.Print(0.0);
            carPositions.Add(carPosition);
        }
    }

    private void LoadTrackOffline()
    {
        string raceString = File.ReadAllText(".." + Bot.pathSeparator + "tracks" + Bot.pathSeparator + "keimola.track");
        Race race = JsonConvert.DeserializeObject<Race>(raceString);
        Assert(race.track.id == "keimola");
        Console.WriteLine("Offline track loading succeeded");

        int laneIndex = 0;
        foreach(var lane in race.track.lanes)
        {
            Assert(lane.index == laneIndex);
            laneIndex++;
        }
    }

    private void TestShortestPath()
    {
        string raceString = File.ReadAllText(".." + Bot.pathSeparator + "tracks" + Bot.pathSeparator + "keimola.track");
        Race race = JsonConvert.DeserializeObject<Race>(raceString);
        race.Initialize();
        Assert(race.track.id == "keimola");

        // Does not work currently, to be continued
        var track = race.track;
        var startVertex = Bot.GetShortestPath(track, track.pieces[0], track.lanes[0], track.pieces[1], track.lanes[0], null);
        Assert(startVertex != null);
        Assert(startVertex.Next != null);
        Assert(startVertex.Next.Lane.index == 0);
        Assert(startVertex.Next.Piece.Index == 1);

        startVertex = Bot.GetShortestPath(track, track.pieces[0], track.lanes[0], track.pieces[2], track.lanes[0], null);
        Assert(startVertex != null);
        Assert(startVertex.Next != null);
        Assert(startVertex.Next.Lane.index == 0);
        Assert(startVertex.Next.Piece.Index == 1);
        Assert(startVertex.Next.Next.Lane.index == 0);
        Assert(startVertex.Next.Next.Piece.Index == 2);

        startVertex = Bot.GetShortestPath(track, track.pieces[0], track.lanes[0], track.pieces[track.pieces.Length - 1], track.lanes[0], null);
        Assert(startVertex != null);
        Assert(startVertex.Next.Next.Next.Lane.index == 0);
        Assert(startVertex.Next.Next.Next.Piece.Index == 3);
        Assert(startVertex.Next.Next.Next.Next.Lane.index == 1);
        Assert(startVertex.Next.Next.Next.Next.Piece.Index == 4);

        startVertex = Bot.GetShortestPath(track, track.pieces[0], track.lanes[1], track.pieces[track.pieces.Length - 1], track.lanes[0], null);
        Assert(startVertex != null);
        Assert(startVertex.Next.Next.Next.Lane.index == 1);
        Assert(startVertex.Next.Next.Next.Piece.Index == 3);
        Assert(startVertex.Next.Next.Next.Next.Lane.index == 1);
        Assert(startVertex.Next.Next.Next.Next.Piece.Index == 4);

        Assert(track.pieces.Length == 40);
        Assert(track.pieces[35].hasSwitch == true);
        startVertex = Bot.GetShortestPath(track, track.pieces[35], track.lanes[1], track.pieces[34], track.lanes[1], null);
        Assert(startVertex.Lane.index == 1);
        Assert(startVertex.Next.Lane.index == 1);

        // Test banned path
        startVertex = Bot.GetShortestPath(track, track.pieces[0], track.lanes[0], track.pieces[track.pieces.Length - 1], track.lanes[0], new KeyValuePair<Track.Piece, Track.Lane>(track.pieces[4], track.lanes[1]));
        Assert(track.pieces[3].hasSwitch == true);
        Assert(startVertex.Next.Next.Next.Lane.index == 0);
        Assert(startVertex.Next.Next.Next.Piece.Index == 3);
        Assert(startVertex.Next.Next.Next.Next.Lane.index == 0);
        Assert(startVertex.Next.Next.Next.Next.Piece.Index == 4);
    }

    private void TestTurboFinder()
    {
        string raceString = File.ReadAllText(".." + Bot.pathSeparator + "tracks" + Bot.pathSeparator + "keimola.track");
        Race race = JsonConvert.DeserializeObject<Race>(raceString);
        var straights = Bot.FindLongestStraights(race.track);

        foreach(var x in straights)
        {
            Console.WriteLine("Start index: " + x.index + ", straight length: " + x.length);
        }
        Assert(straights.Count >= 3);
        Assert(straights[0].index == 35);

        // 9 and 12 are the same length
        Assert(straights[1].index == 12 || straights[1].index == 9);
        Assert(straights[2].index == 9 || straights[2].index == 12);
        Assert(straights[1].index != straights[2].index);
    }

    private void Assert(bool condition)
    {
        if (!condition)
        {
            Console.WriteLine("ASSERT FAILED!");
            throw new Exception("ASSERT FAILED!");
        }
    }

    private void TestSimulator()
    {
        string raceString = File.ReadAllText(".." + Bot.pathSeparator + "tracks" + Bot.pathSeparator + "keimola.track");
        Race race = JsonConvert.DeserializeObject<Race>(raceString);
        Bot bot = new Bot();
        bot.race = race;

        Assert(bot.WillItCrashWithConstantSpeed(race.track, 4, 0.0, 0, 0.0, 0.0, 0.0, 6.5) == false);
        Assert(bot.WillItCrashWithConstantSpeed(race.track, 4, 0.0, 0, 0.0, 0.0, 0.0, 8.2) == true);
    }
}

