using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Track
{
    public Piece GetNextPiece(int pieceIndex)
    {
        return pieces[(pieceIndex + 1) % pieces.Length];
    }

    public Track.Piece GetMergedPiece(Track.Piece piece)
    {
        var currentPiece = piece;
        var pieceIndex = piece.Index;
        while (true)
        {
            var nextPiece = this.pieces[(pieceIndex + 1) % pieces.Length];
            if (currentPiece.IsStraight && nextPiece.IsStraight)
            {
                var mergedPiece = currentPiece.Clone();
                mergedPiece.length = currentPiece.length + nextPiece.length;
                mergedPiece.mergedPieceEndIndex = nextPiece.Index;
                currentPiece = mergedPiece;
            }
            else if (currentPiece.IsCorner && nextPiece.IsCorner && currentPiece.radius == nextPiece.radius)
            {
                var mergedPiece = currentPiece.Clone();
                mergedPiece.angle = currentPiece.angle + nextPiece.angle;
                mergedPiece.mergedPieceEndIndex = nextPiece.Index;
                currentPiece = mergedPiece;
            }
            else
            {
                break;
            }
            pieceIndex++;
        }
        return currentPiece;
    }

    public class Piece
    {
        public double? length;

        [JsonProperty("switch")]
        public bool? hasSwitch;

        public double? radius;
        public double? angle;
        public int Index;

        [JsonIgnoreAttribute]
        public int? mergedPieceEndIndex;

        public Piece Clone()
        {
            var clone = new Piece();
            clone.length = this.length;
            clone.hasSwitch = this.hasSwitch;
            clone.radius = this.radius;
            clone.angle = this.angle;
            clone.Index = this.Index;
            return clone;
        }

        public bool IsStraight
        {
            get
            {
                return length.HasValue;
            }
        }

        public bool IsCorner
        {
            get
            {
                return !IsStraight;
            }
        }
    }

    public class Lane
    {
        public double distanceFromCenter;
        public int index;
    }

    public string id;
    public string name;
    public Piece[] pieces;
    public Lane[] lanes;
    public Object startingPosition; // Unnecessary visual cue.
    private double? lapCenterlineLength;

    public void Print()
    {
        Console.WriteLine("Track: " + id + "/" + name);
        for(int i = 0; i < lanes.Length; ++i)
        {
            var lane = lanes[i];
            Console.WriteLine("Lane " + i + "# distanceFromCenter: " + lane.distanceFromCenter + ", index: " + lane.index);
        }
        for(int i = 0; i < pieces.Length; ++i)
        {
            var piece = pieces[i];
            string lengths = "";
            for(int j = 0; j < lanes.Length; ++j)
            {
                double length = GetPieceLength(i, lanes[j].index, lanes[j].index);
                lengths += length + " ";
            }
            string message = String.Format("Piece {0,2}#  length {1,3}  switch {2,4}  radius {3,4}  angle {4,6}  lengths {5}", i, piece.length, piece.hasSwitch, piece.radius, piece.angle, lengths);
            Console.WriteLine(message);
        }
    }

    public double GetLapCenterlineLength()
    {
        if (lapCenterlineLength == null)
        {
            var piecesInLap = this.pieces.Length;
            double distance = 0.0;
            for (int i = 0; i < piecesInLap; ++i)
            {
                distance += GetPieceLengthCenter(i);
            }
            lapCenterlineLength = distance;
        }
        return lapCenterlineLength.Value;
    }

    public double GetLapCenterlineDistanceFromRaceDistance(double raceDistance)
    {
        return raceDistance % GetLapCenterlineLength();
    }

    public double GetLaneOffset(int laneIndex)
    {
        foreach(Lane lane in lanes)
        {
            if(lane.index == laneIndex)
            {
                return lane.distanceFromCenter;
            }
        }
        return 0.0;
    }

    public double GetPieceAngle(int pieceIndex)
    {
        var piece = pieces[pieceIndex];
        double angle = piece.angle ?? 0.0;
        return angle;
    }

    public double GetPieceRadius(int pieceIndex, int laneIndex)
    {
        var piece = pieces[pieceIndex];
        if(piece.radius != null)
        {
            double angle = (double)piece.angle;
            double radius = (double)piece.radius;
            double laneOffset = GetLaneOffset(laneIndex);
            double offset = (angle < 0.0 ? laneOffset : -laneOffset);
            double adjustedRadius = radius + offset;
            return adjustedRadius;
        }
        else
        {
            return 0.0;
        }
    }

    public static double ConvertToRadians(double angle)
    {
        return (Math.PI / 180) * angle;
    }

    public double GetPieceLength(Piece piece, int startLaneIndex, int endLaneIndex)
    {
        if(piece.IsStraight)
        {
            if (startLaneIndex == endLaneIndex)
            {
                return (double)piece.length;
            }
            else
            {
                var startLane = this.lanes[startLaneIndex];
                var endLane = this.lanes[endLaneIndex];

                var x = (double)piece.length;
                var y = startLane.distanceFromCenter - endLane.distanceFromCenter;
                return Math.Sqrt(x * x + y * y);
            }
        }
        else
        {
            if (startLaneIndex == endLaneIndex)
            {
                double angle = (double)piece.angle;
                double radius = (double)piece.radius;
                double laneOffset = GetLaneOffset(startLaneIndex);
                double offset = (angle < 0.0 ? laneOffset : -laneOffset);
                double adjustedRadius = radius + offset;

                var result = Math.Abs(angle) / 360.0 * 2.0 * Math.PI * adjustedRadius;
                return result;
            }
            else
            {
                // Approx in parts
                double x, y;
                double previousX, previousY;

                var startLaneRadius = GetPieceRadius(piece.Index, startLaneIndex);
                var endLaneRadius = GetPieceRadius(piece.Index, endLaneIndex);

                previousX = startLaneRadius;
                previousY = 0;

                var pieceAngle = GetPieceAngle(piece.Index);
                int steps = 4;
                double length = 0.0;
                for (int i = 0; i < steps; ++i)
                {
                    double percent = (i + 1.0) / steps;
                    var iteratedRadius = startLaneRadius + percent * (endLaneRadius - startLaneRadius);
                    var iteratedAngle = pieceAngle * percent;
                    x = Math.Cos(ConvertToRadians(iteratedAngle)) * iteratedRadius;
                    y = Math.Sin(ConvertToRadians(iteratedAngle)) * iteratedRadius;

                    var stepX = (x - previousX);
                    var stepY = (y - previousY);

                    length += Math.Sqrt(stepX * stepX + stepY * stepY);

                    previousX = x;
                    previousY = y;
                }

                return length;
            }
        }
    }

    public double GetPieceLength(int pieceIndex, int startLaneIndex, int endLaneIndex)
    {
        var piece = pieces[pieceIndex];
        return GetPieceLength(piece, startLaneIndex, endLaneIndex);
    }

    public double GetPieceLengthCenter(int pieceIndex)
    {
        var piece = pieces[pieceIndex];

        if (piece.length != null)
        {
            return (double)piece.length;
        }
        else
        {
            double angle = (double)piece.angle;
            double radius = (double)piece.radius;

            return Math.Abs(angle) / 360.0 * 2.0 * Math.PI * radius;
        }
    }

    public double GetDistance(PiecePosition oldPosition, PiecePosition newPosition)
    {
        return GetDistance(oldPosition.pieceIndex, newPosition.pieceIndex, oldPosition.inPieceDistance, newPosition.inPieceDistance, oldPosition.lane.startLaneIndex, newPosition.lane.startLaneIndex);
    }

    public double GetDistance(int pieceIndex1, int pieceIndex2, double inPieceDistance1, double inPieceDistance2, int laneIndex1, int laneIndex2)
    {
        // TODO: how to calculate when laneIndex1 != laneIndex2?
        if(pieceIndex1 == pieceIndex2)
        {
            return inPieceDistance2 - inPieceDistance1;
        }
        else
        {
            double pieceLength1 = GetPieceLength(pieceIndex1, laneIndex1, laneIndex2);
            double piece1 = pieceLength1 - inPieceDistance1;

            double middle = 0.0;
            int n = pieces.Length;
            for(int i = (pieceIndex1 + 1) % n; i != pieceIndex2; i = (i + 1) % n)
            {
                middle += GetPieceLength(i, laneIndex1, laneIndex2);
            }

            double piece2 = inPieceDistance2;

            return piece1 + middle + piece2;
        }
    }

    public void Initialize()
    {
        int index = 0;
        foreach (var piece in pieces)
        {
            piece.Index = index++;
        }
    }
}

public class Id
{
    public string name;
    public string color;
};

public class Race
{
    // Suppress "is never assigned" as fields are assigned by reflection
    #pragma warning disable 0649

    public class RaceSession
    {
        public bool IsQualification
        {
            get
            {
                return durationMs.HasValue;
            }
        }

        public int? laps;
        public int? maxLapTimeMs;
        public bool? quickRace;
        public int? durationMs;
    }

    public class Car
    {
        public Id id;
        public Object dimensions;
    }

    public Track track;
    public Car[] cars;
    public RaceSession raceSession;
    #pragma warning restore 0649

    public void Initialize()
    {
        track.Initialize();
    }

    public void Print()
    {
        track.Print();

        for(int i = 0; i < cars.Length; ++i)
        {
            var car = cars[i];
            Console.WriteLine("Car " + i + "# " + car.id.name + "/" + car.id.color);
        }
        Console.WriteLine("Laps: " + raceSession.laps);
        Console.WriteLine("maxLapTimeMs: " + raceSession.maxLapTimeMs);
        Console.WriteLine("quickRace: " + raceSession.quickRace);
        Console.WriteLine("durationMs: " + raceSession.durationMs);
    }
}

public class Lane
{
    public int startLaneIndex;
    public int endLaneIndex;
}

public class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;
    public int lap;
}

public class TurboBoost
{
    public int duration;
    public bool active;
    public double factor;
    public bool triedToActivate;
}

public class CarPosition
{
    public double angle;

    public Id id;

    public PiecePosition piecePosition;

    public double GetLapDistanceTravelled(Track track)
    {
        return GetRaceDistanceTravelled(track, 0);
    }

    public double GetRaceDistanceTravelled(Track track)
    {
        return GetRaceDistanceTravelled(track, piecePosition.lap);
    }

    public double GetRaceDistanceTravelled(Track track, int lap)
    {
        var piecesInLap = track.pieces.Length;

        var fullPiecesTravelled = lap * piecesInLap + piecePosition.pieceIndex;

        double distance = 0.0;
        for (int i = 0; i < fullPiecesTravelled; ++i)
        {
            var pieceIndex = i % piecesInLap;
            distance += track.GetPieceLengthCenter(pieceIndex);
        }

        // Current piece
        distance += piecePosition.inPieceDistance;

        return distance;
    }

    [JsonIgnoreAttribute]
    public double speed = 0.0;

    [JsonIgnoreAttribute]
    public double previousSpeed = 0.0;

    [JsonIgnoreAttribute]
    public int previousPieceIndex = 0;

    [JsonIgnoreAttribute]
    public int previousStartLane = 0;

    public double DeltaSpeed
    {
        get
        {
            return speed - previousSpeed;
        }
    }


    [JsonIgnoreAttribute]
    public double previousAngle = 0.0;

    [JsonIgnoreAttribute]
    public double previousDeltaAngle = 0.0;

    [JsonIgnoreAttribute]
    public double previousDeltaDeltaAngle = 0.0;

    public double DeltaAngle
    {
        get
        {
            return angle - previousAngle;
        }
    }

    public double DeltaDeltaAngle
    {
        get
        {
            return DeltaAngle - previousDeltaAngle;
        }
    }

    [JsonIgnoreAttribute]
    public double travelledDistance = 0.0;

    [JsonIgnoreAttribute]
    public bool crashed = false;

    [JsonIgnoreAttribute]
    public int bestLapTimeMs = Int32.MaxValue;

    [JsonIgnoreAttribute]
    public List<TurboBoost> turbos = new List<TurboBoost>();

    public int TurbosAvailable()
    {
        int sum = 0;
        foreach(var t in turbos)
        {
            if(!t.active && !t.triedToActivate)
            {
                ++sum;
            }
        }
        return sum;
    }
        
    public TurboBoost AvailableTurbo
    {
        get
        {
            return turbos.FirstOrDefault(x => !x.active && !x.triedToActivate);
        }
    }

    public double CurrentTurboFactor()
    {
        double factor = 1.0;
        foreach(var t in turbos)
        {
            if(t.active)
            {
                factor *= t.factor;
            }
        }
        return factor;
    }

    public void Crash()
    {
        crashed = true;
        // Even unactivated turbos are lost on crash!
        turbos.Clear();
    }

    public void StartTurbo()
    {
        foreach(var t in turbos)
        {
            if(!t.active)
            {
                t.active = true;
            }
        }
        //Bot.Log("Model error: couldn't find turbo to start!");
    }

    public void EndTurbo()
    {
        if(turbos.Count == 0 || !turbos[0].active)
        {
            Bot.Log("Model error: couldn't find turbo to end!");
        }
        else
        {
            Bot.Log("Removing used turbo with duration: " + turbos[0].duration);
            turbos.RemoveAt(0);
        }
    }

    public void AddTurbo(int duration, double factor)
    {
        if(crashed)
        {
            // Crashed cars lose chance to turbo!
            return;
        }

        // Unused turbos are lost!
        turbos = turbos.Where(x => x.active).ToList();

        var t = new TurboBoost();
        t.active = false;
        t.duration = duration;
        t.factor = factor;
        turbos.Add(t);
    }

    [JsonIgnoreAttribute]
    public List<double> RaceDistanceTravelledOnTicks;

    public void Print(double throttle)
    {
        string message = String.Format("Car position: {0,12} / {1,-6}  dist {2,-8:0.0000}  piece {3,2}  lap {4}  angle {5,-8:0.0000}  lane {6}->{7} thro {8:0.0000} speed {9,-8:0.0000}  crash {10}  turboF {11}  turbos {12}",
                id.name,
                id.color,
                piecePosition.inPieceDistance,
                piecePosition.pieceIndex,
                piecePosition.lap,
                angle,
                piecePosition.lane.startLaneIndex,
                piecePosition.lane.endLaneIndex,
                throttle,
                speed,
                crashed,
                CurrentTurboFactor(),
                TurbosAvailable());

        Console.WriteLine(message);
    }
}

