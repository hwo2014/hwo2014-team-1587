#!/bin/bash

FILE=$1

gnuplot -e "plot '$FILE' using 2 title 'throttle', '$FILE' using 3 title 'speed' with lines, '$FILE' using 7 title 'nextSpeed' with lines" -p
gnuplot -e "plot '$FILE' using 4 title 'angle' with lines, '$FILE' using 5 title 'radius' with lines, '$FILE' using 8 title 'nextAngle' with lines" -p
